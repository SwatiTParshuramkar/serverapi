

const  express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

require('../db/conn');
const User = require("../model/userSchema");



router.get('./',(req,res)=>{
    res.send(`Hello world from server auth.js`);
});




/* Using Promises.

router.post('/register',(req,res)=>{
    
    //it's a validation if any property unfilled.
    const { name, email,phone, work, password,cpassword} = req.body; 

    if(!name || !email || !phone || !work || !password || !cpassword){
        return res.status(422).json({error : "Please filled the Field property."})
    }

    // console.log(name);
    // console.log(email);
    // console.log(work);
    // // res.json({message:req.body});
    // // res.send('my register page');


    
    User.findOne({email:email})
    // left side wala database ka email hai aur right side ka user jo dega wo email hoga.
    .then((userExist)=>{
        if(userExist){
            return res.status(422).json({error : "Email already Exist."})
        }
        const user = new User({name, email,phone, work, password,cpassword});
        user.save().then(()=>{
            res.status(201).json({message:"user registered successfully."});
        }).catch((err)=>res.status(500).json({error:"failed to registered"}));
        
        
    }).catch(err =>{console.log(err);});


});
*/

// Async-Await.

router.post('/register',async(req,res)=>{
    
    //it's a validation if any property unfilled.
    const { name, email,phone, work, password,cpassword} = req.body; 
    
    //every field should be filled

    if(!name || !email || !phone || !work || !password || !cpassword){
        return res.status(422).json({error : "Please filled the Field property."});
    }

    /* if user already exists in the database then should be notify that email already exist. if password not matched with confirm password then also its shows the error.*/
    /* If not then create new user instance and fill the all property.
     and send the response that user registered successfully and stored the data mongodb.*/


    try {

        const userExist = await User.findOne({email:email});

        if(userExist){
            return res.status(422).json({error : "Email already Exist."});
        }else if(password != cpassword){
            return res.status(422).json({error : "Password are not matching."});
        }else{
        const user = new User({name, email,phone, work, password,cpassword});

         await user.save();

        res.status(201).json({message:"user registered successfully."});

        }    
        // otherwise catch the error
        } catch(err){
        console.log(err);        
    }

       
});

//login  route

router.post('/signin',async(req,res)=>{
    // console.log(req.body);
    // res.json({message:"awsome"});

    try{
        const {email ,password} = req.body;
        
        //if any one of the field empty then shows response please filled the data.

        if(!email || !password){
            return res.status(400).json({error:"please filled the data"});
        }

        // if user enter mail and database stored mail fine similar then user login.
        const userLogin = await User.findOne({email:email});
        // console.log(userLogin);
        if (userLogin) {
            const isMatch = await bcrypt.compare(password,userLogin.password);

            const token = await userLogin.generateAuthToken();//133 line
            console.log(token);
            //cookies with the name of jwtoken and set the expires date, in httponly also true.
            // res.cookie("jwtoken",token,{
            //     expires:new Date(Date.now()+25892000000),
            //     httpOnly:true
            // });
           

        // user log in credientials incorrect then user error otherwise sign in successfully.
        if(!isMatch){
            res.status(400).json({message:"Invalid credientials password"})

        }else{
            res.status(200).json({message:"user signin successfully"})
            // passed the token and the response, that we make

        }
            
        } else {
            res.status(400).json({message:"Invalid credientials"})
            
        }        

    }catch(err){
        console.log(err)

    }
});


module.exports = router;