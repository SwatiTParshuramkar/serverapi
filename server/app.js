const dotenv = require('dotenv');

const mongoose = require('mongoose');

const express = require('express');
const app = express();

dotenv.config({path:"./config.env"});

//connection is from db/conn.js for connect database.

require('./db/conn');
// const User = require('./model/userSchema');

app.use(express.json());


/* we link the router files to make our route easy.*/


app.use(require('./router/auth'));

// connect to the mongodb Database.
//process.env keeps data secure.


const PORT = process.env.PORT;




//Middelware.

const middleware = (req,res,next)=>{
    console.log(`Hello my middleware`);
    next();
}



// '/' is mentioned that what kind of root you want to provide.

app .get('/',(req,res)=>{
    // we are sending response to server.
    res.send('Hello World from the server app.js')
   
});

app .get('/about',middleware,(req,res)=>{
    // we are sending response to server.
    res.send('Hello World from  About the server')
});

app .get('/contact',(req,res)=>{
    // we are sending response to server.
    res.send('Hello World from contact the server')
});

app .get('/signin',(req,res)=>{
    // we are sending response to server.
    res.send('Hello World from  sign in the server')
});

app .get('/signup',(req,res)=>{
    // we are sending response to server.
    res.send('Hello World from  sign Up the server')
});


// our server is running on port 3000.
// user visit the perticuler the port then we should know the in which port we are.

app.listen(PORT,()=>{
    console.log(`Server is running at port no ${PORT} `);
});