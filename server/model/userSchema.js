/**
 * In this file document structures have what we want in the field.
 */


const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');



// userschema  to define the database structures.
// jaise ki dabase pr apka jo bhi data hai wo koun koun sa aur kis tarika ka hai.


const userSchema = new mongoose.Schema({
    name : {
        type :String,
        required :true
    },
    email:{
        type :String,
        required :true
    },
    phone :{
        type :Number,
        required :true
    },
    work :{
        type :String,
        required :true
    },
    password :{
        type :String,
        required :true
    },
    cpassword :{
        
    },
    tokens :[
        {
            token:{
                type :String,
                required :true

            }
        }
    ]
    

})



// we are  hashing the password.

userSchema.pre('save', async function (next) {
    console.log('hi from bcrypt notation');
    if (this.isModified('password')) {

        this.password = await bcrypt.hash(this.password,12);
        this.cpassword = await bcrypt.hash(this.cpassword,12);
    }
    next();
});

//we are generating token
userSchema.methods.generateAuthToken =async function(){
    try{
        let token = jwt.sign({_id:this._id},process.env.SECRET_KEY);
        this.tokens = this.tokens.concat({token:token});
        await this.save();// do not need tosave.
        return token;

        // add expires token 24hr. 

    }catch(err){
        console.log(err);
    }
}

//Database collection creation.

const User = mongoose.model('REGISTRATION',userSchema);

module.exports = User;

